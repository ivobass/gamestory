#pragma once
#include "infojogo.h"
#include <string>
#include <vector>

 class InfoJogoBasquetebol : public InfoJogo  {
 public:
//   InfoJogoBasquetebol();
InfoJogoBasquetebol (std::string namePlayer, std::string posPlayer, int scorePlayer, int timePlayer,
                  std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB);

//Getters
    std::string getNamePlayer() const;
    std::string getPosPlayer() const;
    int getScorePlayer() const;
    int getTimePlayer() const;

    void imprimeJogo() const;
    void validar();

 private:
   std::string m_namePlayer;
   std::string m_posPlayer;
   int m_scorePlayer;
   int m_timePlayer;

   //variaveis
//     std::string m_nameTeamA;
//     std::string m_nameTeamB;
//     int m_scoreTeamA;
//     int m_scoreTeamB;
     int m_total;
     std::string m_players;
     std::string m_changes;
     int m_playerScore;
     int m_playerTime;
 };




