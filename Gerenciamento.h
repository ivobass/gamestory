#pragma once

#include <iostream>
#include <limits>
#include <string>
#include <iomanip>


class Gerenciamento {
public:
    Gerenciamento();
    //Impressão do Menu para escolher opção
    int Menu();
    //Função que permite garantir que a escolha da opção é efetuada com sucesso / devolve opção escolhida
    int OpMenu();
    //Efetua ação consoante opção escolhida
    void SelecOpMenu(int valor);
    void StartGame();
    //validar modalidade futebol ou basquetebol
    int selectModality();
    //introduzir dados de diferentes modalidades
    void inputDataFut();
    void inputDataBask();
    //ler dados via ficheiro de diferentes modalidades
    void readDataFut();
    void readDataBask();
    int typeData();
    int typePlayer();
    void showData();
    void suPlayers();

protected:

      std::string m_nameTeamA;
      std::string m_nameTeamB;
      int m_numberPlayer;
      std::string m_player_name;
      std::string m_player_position;
      size_t m_player_score;
      size_t m_scored_time;
      size_t m_scoreTeamA;
      size_t m_scoreTeamB;
      // variaveis para ler de dados
       int numberPlayer;
       std::string namePlayer;
       std::string posPlayer;
       int scorePlayer;
       int timePlayer;
       std::string nameTeamA;
       std::string nameTeamB;
       int scoreTeamA;
       int scoreTeamB;
       std::string m_supTeamA;
};


