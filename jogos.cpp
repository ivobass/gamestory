#include "jogos.h"
#include <iostream>
#include <algorithm>

Jogos::Jogos(){

}

//valida jogadores titulares Equipa A
void Jogos::adicionarFut(std::unique_ptr<InfoJogoFutebol> valor) {

  m_jogosFutebolA.push_back(std::move(valor));   // validar ou introduzir jogadores move indica ao compilador que ele não quer uma cópia, mas sim, mover o elemento.
}

//introduzir jogadores titulares Equipa A
std::vector<InfoJogoFutebol> Jogos::readTeamA(const std::string &nameTeamA, size_t team_size){
    std::vector<InfoJogoFutebol> teamInfoA;
    std::cout << "Entre o jogadores Titulares da equipa A: " << nameTeamA << ":" << std::endl;
  m_numberPlayer=1;
for (size_t i=0; i< team_size; i++) {
    team_size=11;// definimos os 11 titulares
    std::cout  << "Jogador numero # "<< m_numberPlayer << std::endl;
    m_numberPlayer=m_numberPlayer+1;
    std::cout << "  Nome do jogador : ";
    std::cin >> m_namePlayer;
    std::cout << "  Posição do jogador (guarda-redes, defesa, médio, avançado) : ";
    std::cin >> m_posPlayer;
//    std::cout << "  Golos marcados por o jogador #" << namePlayer << ":"<< std::endl;
//    std::cin >> scorePlayer;
//    std::cout << "  Tempo que o jogador Marcou :";
//    std::cin >> timePlayer;
    std::cout << std::endl;


    teamInfoA.emplace_back( m_numberPlayer, m_namePlayer,  m_posPlayer,  m_scorePlayer,  m_timePlayer
                          ,m_nameTeamA, m_nameTeamB,  m_scoreTeamA,  m_scoreTeamB);

}
    return teamInfoA;
}

//jogadores suplentes Equipa A

std::vector<InfoJogoFutebol> Jogos::readSupA(const std::string &supTeamA, size_t sup_size){
    std::vector<InfoJogoFutebol> supInfoA;

    std::cout << "Entre o jogadores Suplentes da equipa" << supTeamA << ":" << std::endl;
    m_numberPlayer=12;
for  (size_t i=0; i< sup_size; i++) {
    sup_size=7;// definimos os 7 titulares
    std::cout  << "Jogador numero # "<< m_numberPlayer << std::endl;
    m_numberPlayer=m_numberPlayer+1;
    std::cout << "  Nome do jogador : ";
    std::cin >> m_namePlayer;
    std::cout << "  Possição do jogador (guarda-redes, defesa, médio, avançado) : ";
    std::cin >> m_posPlayer;
//    std::cout << "  Golos marcados por o jogador #" << namePlayer << ":"<< std::endl;
//    std::cin >> scorePlayer;
//    std::cout << "  Tempo que o jogador Marcou :";
//    std::cin >> timePlayer;
    std::cout << std::endl;


     supInfoA.emplace_back( m_numberPlayer, m_namePlayer, m_posPlayer, m_scorePlayer, m_timePlayer
                          ,m_nameTeamA, m_nameTeamB, m_scoreTeamA, m_scoreTeamB);

}
    return supInfoA;
}




//introduzir jogadores titulares Equipa B
std::vector<InfoJogoFutebol> Jogos::readTeamB(const std::string &nameTeamA, size_t team_size){
    std::vector<InfoJogoFutebol> teamInfoB;
    std::string namePlayer;
    std::string posPlayer;
    int scorePlayer;
    int timePlayer;

    std::cout << "Entre o jogadores Titualres da equipa B: " << nameTeamB << ":" << std::endl;
    m_numberPlayer=1;
for (size_t numberPlayer=1; numberPlayer< team_size; numberPlayer++) {
    team_size=11;// definimos os 11 titulares
    std::cout  << "Jogador numero # "<< numberPlayer << std::endl;
    m_numberPlayer=m_numberPlayer+1;
    std::cout << "  Nome do jogador : ";
    std::cin >> namePlayer;
    std::cout << "  Possição do jogador (guarda-redes, defesa, médio, avançado) : ";
    std::cin >> posPlayer;
//    std::cout << "  Golos marcados por o jogador #" << namePlayer << ":"<< std::endl;
//    std::cin >> scorePlayer;
//    std::cout << "  Tempo que o jogador Marcou :";
//    std::cin >> timePlayer;
    std::cout << std::endl;


    teamInfoB.emplace_back( numberPlayer, namePlayer,  posPlayer,  scorePlayer,  timePlayer
                          ,nameTeamA, nameTeamB,  scoreTeamA,  scoreTeamB);

}
    return teamInfoB;
}





////valida jogadores titulares basquetebol
//void Jogos::adicionarBask(std::unique_ptr<InfoJogoBasquetebol>valor) {
//  m_jogosBasquetebol.push_back(std::move(valor));
//}

//ordena jogadores futebol
void Jogos::ordenaFut(){
    std::sort(std::begin(m_jogosFutebolA),std::end(m_jogosFutebolA));

}
//ordena jogadores basquetebol
void Jogos::ordenaBask(){
    std::sort(std::begin(m_jogosBasquetebol),std::end(m_jogosBasquetebol));

}
//lee os dados dos jogadores de futebol
void Jogos::imprimeFut() {
  for (auto& valoratual : m_jogosFutebolA) {
    valoratual->imprimeJogo();
    std::cout << "tamanho do vector" << m_jogosFutebolA.size();
  }
  return;
}
//lee os dados dos jogadores de basquetebol
void Jogos::imprimeBask() {
  for (auto& valoratual : m_jogosBasquetebol) {
    valoratual->imprimeJogo();
  }
  return;
}


//Menu principal
//void Jogos::menu(){
//       int op_menu=1;
//    do {
//        //        system("clear");
//        std::cout << "====================================================" << std::endl;
//        std::cout << "*       AF2 - 21093 - Programação por Objectos     *" << std::endl;
//        std::cout << "====================================================" << std::endl;
//        std::cout << "*                                                  *" << std::endl;
//        std::cout << "*           ...Menu Principal...                   *" << std::endl;
//        std::cout << "*                                                  *" << std::endl;
//        std::cout << "*        1. Introduzir modalidade e dados do jogo  *" << std::endl;
//        std::cout << "*        2. Informações Jogo Futebol               *" << std::endl;
//        std::cout << "*        3. Informações Jogo Basquetebol           *" << std::endl;
//        std::cout << "*        4. Listagem de Resultados de Futebol      *" << std::endl;
//        std::cout << "*        5. Listagem de Resultados de Basquetebol  *" << std::endl;
//        std::cout << "*        0. Sair                                   *" << std::endl;
//        std::cout << "====================================================" << std::endl;
//        std::cout << "Introduzir a sua opção:"<< std::endl;
//        std::cin >> op_menu;

//        } while(op_menu !=0);
//}

