#pragma once
#include "infojogo.h"
#include <string>
#include <vector>

class InfoJogoFutebol : public InfoJogo {
public:
InfoJogoFutebol (int numberPlayer, std::string &namePlayer, std::string &posPlayer, int scorePlayer, int timePlayer
                 ,std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB);
//Getters
int getNumberPlayer();
 const std::string &getNamePlayer();
 const std::string &getPosPlayer();
 int getScorePlayer();
 int getTimePlayer();
 void getOutPlayer();
 void imprimeJogo();
 void printPlayer();
 void validar();
 int GetNumber();

protected:


  std::string m_namePlayer;
  std::string m_posPlayer;
  int m_scorePlayer;
  int m_timePlayer;
  int m_numberPlayer{-1};
  // variaveis para ler de dados
   int numberPlayer;
   std::string namePlayer;
   std::string posPlayer;
   int scorePlayer;
   int timePlayer;
   std::string numero;

//   std::string nameTeamA;
//   std::string nameTeamB;
//   int scoreTeamA;
//   int scoreTeamB;

};

std::ostream &operator<<(std::ostream &os, const std::vector<InfoJogoFutebol> &other);

//std::vector<InfoJogoFutebol> readTeam (const std::string &nameTeam, size_t team_size);
std::vector<InfoJogoFutebol> InputTeamPlayers(const std::string &nameTeam, size_t sizeTeam);

std::ostream& Expulso(std::ostream& out);
