#include "Gerenciamento.h"
#include "jogos.h"
#include "infojogo.h"
#include "infojogofutebol.h"
#include <iostream>
#include <string>
#include <fstream>



Gerenciamento::Gerenciamento(){

}

//Impressão do Menu para escolher opção
int Gerenciamento::Menu(){
    int opmenu;
    //system("clear");
    std::cout << "====================================================" << std::endl;
    std::cout << "*       AF2 - 21093 - Programação por Objectos     *" << std::endl;
    std::cout << "====================================================" << std::endl;
    std::cout << "*                                                  *" << std::endl;
    std::cout << "*           ...Menu Principal...                   *" << std::endl;
    std::cout << "*                                                  *" << std::endl;
    std::cout << "*        1. Introduzir modalidade e dados do jogo  *" << std::endl;
    std::cout << "*        2. Ler ficheiro txt com dados do jogo     *" << std::endl;
    std::cout << "*        3. Adicionar score e tempos               *" << std::endl;
    std::cout << "*        4. Registar substituições de jogadores    *" << std::endl;
    std::cout << "*        5. Registar expulsões de jogadores        *" << std::endl;
    std::cout << "*        6. Registar autogolos no futebol          *" << std::endl;
    std::cout << "*        7. Listagem de Resultados de Futebol      *" << std::endl;
    std::cout << "*        8. Listagem de Resultados de Basquetebol  *" << std::endl;
    std::cout << "*        9. Sair                                   *" << std::endl;
    std::cout << "====================================================" << std::endl;

    do{
        std::cout << std::endl << "Selecione a sua opcão de entrada:: "; //solicitamos a opcao
        std::cin >> opmenu;   //obtemos a opcao do input

        //se a opcao nao estiver no intervalo permitido
        //avisamos o utilizador e limpamos o buffer da entrada para evitar erros
        if(opmenu<1 || opmenu>9){
            std::cout << "Introduza uma opcao valida! Entre 1 e 9." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

    }while(opmenu<1 || opmenu>9);

    //limpamos o buffer da entrada para evitar erros
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return opmenu; //devolvemos a opcao escolhilda
}

//////Função que permite garantir que a escolha da opção é efetuada com sucesso / devolve opção escolhida
//int Gerenciamento::OpMenu(){
//    int op{-1};
//    std::cout << "Entre a sua Opção:" << std::endl;
//    std::cin >> op;
//    return op;
//}


//Efetua ação consoante opção escolhida
void Gerenciamento::StartGame(){
    int op = -1;
    int mod{-1};
    Jogos j;
   do{
        op = Menu();
        switch(op)
        {
        case 1:   // Introduzir modalidade e dados do jogo
            system("clear");
            std::cout << "Selecionou a opção 1:" << std::endl;
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            mod = selectModality();
            if (mod == 1){

                std::cout << "Introduzir Dados de Futebol" << std::endl;
                inputDataFut();
            } else if (mod == 2){
                std::cout << "Introduzir Dados Basquetebol" << std::endl;
                inputDataBask();
            }
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        case 2:   //Ler ficheiro txt com dados do jogo
           // system("clear");
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            std::cout << "Selecionou a opção 2:" << std::endl;
            mod = selectModality();
            std::cout << "Ler ficheiro txt com dados de futebol"<< std::endl;
            if (mod == 1){
                std::cout << "Ler Dados do ficheiro txt de Futebol" << std::endl;
                readDataFut();
            } else if (mod == 2){
                std::cout << "Ler Dados do ficheiro txt de Basquetebol" << std::endl;
                readDataBask();
            }
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        case 3:  //Adicionar score e tempos
          //system("clear");
          std::cout << std::endl << "-------------------------------------------" << std::endl;
          std::cout << "Selecionou a opção 3:" << std::endl;
          mod = selectModality();
          if (mod == 1){
              std::cout << "Adicionar os golos e tempos do jogadores de futebol" << std::endl;

          } else if (mod == 2){
              std::cout << "Adicionar os pontos e tempos dos jogadores de basquetebol" << std::endl;

          }
          std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        case 4:  //registar substituições de jogadores
            //  system("clear");
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            std::cout << "Selecionou a opção 4:" << std::endl;
            mod = selectModality();
            if (mod == 1){
                std::cout << "registar substituições dos jogadores de futebol" << std::endl;
                showData();
            } else if (mod == 2){
                std::cout << "registar substituições dos jogadores de basquetebol" << std::endl;

            }
            std::cout << std::endl << "-------------------------------------------" << std::endl;

            break;

        case 5:  // registar expulsões de jogadores
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            std::cout << "Selecionou a opção 5:" << std::endl;
            mod = selectModality();
            if (mod == 1){
                std::cout << "registar expulsões dos jogadores de futebol" << std::endl;
                InfoJogoFutebol futebol(numberPlayer,namePlayer,posPlayer,scorePlayer,timePlayer
                                        ,nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
                futebol.getOutPlayer();
            } else if (mod == 2){
                std::cout << "registar expulsões dos jogadores de basquetebol" << std::endl;

            }
            std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;

        case 6:  //registar autogolos no futebol
          std::cout << std::endl << "-------------------------------------------" << std::endl;
          std::cout << "Selecionou a opção 6:" << std::endl;
          std::cout << "registar Autogolos dos jogadores de futebol" << std::endl;
          std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        case 7:  //Listagem de Resultados de Futebol
      std::cout << "Selecionou a opção 7:" << std::endl;
          mod = selectModality();
          if (mod == 1){
              std::cout << "Mostra os resultados de futebol" << std::endl;
              showData();
          } else if (mod == 2){
              std::cout << "Mostra os resultados de basquetebol" << std::endl;

          }
      std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        case 8:  //Listagem de Resultados de Basquetebol
      std::cout << "Selecionou a opção 8:" << std::endl;

      std::cout << std::endl << "-------------------------------------------" << std::endl;
            break;
        default:
            std::cout << std::endl << "Programa realizado por: Ivo Baptista........." << std::endl;
            break;
        }

    }while (op !=9);
    std::cout << std::endl << std::endl << "\t Obrigado por utilizar nosso programa" << std::endl;
}

//seleciona a modalidade do futebol ou basquetebol
int Gerenciamento::selectModality(){
   int modalidade =-1; //opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de Modalidade Futebol ou Basketbol **************" << std::endl;
    std::cout << "1 - Futebol" << std::endl;
    std::cout << "2 - Basquetebol" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

    //ciclo while ate carregar no 3 para sair
    do{
        std::cout << std::endl << "Opcao: ";
        std::cin >> modalidade;   //opção 1, 2 ou 3

        //validar a opção
        //limpamos o buffer de entrada
        if(modalidade<1 || modalidade>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 2." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(modalidade<1 || modalidade>3);

    //limpamos o buffer da entrada
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return modalidade; //retornamos a opção selecionada

}

//seleciona a tipo de dados
int Gerenciamento::typeData(){
   int td =-1; //opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de entrada de Dados Ficheiro ou Teclado **************" << std::endl;
    std::cout << "1 - Ficheiro" << std::endl;
    std::cout << "2 - Teclado" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

   //ciclo while ate carregar no 3 para sair
    do{
        std::cout << std::endl << "Opcao: ";
        std::cin >> td;   //opção 1, 2 ou 3

        //validar a opção
        //limpamos o buffer de entrada
        if(td<1 || td>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 3." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(td<1 || td>3);

    //limpamos o buffer da entrada
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return td; //retornamos a opção selecionada

}

//seleciona a tipo de dados
int Gerenciamento::typePlayer(){
   int td =-1; //opcao invalida

     //apresentamos o menu
    std::cout << "************** Escolha o tipo de jogador a introduzir, Titulares ou Suplentes **************" << std::endl;
    std::cout << "1 - Titulares" << std::endl;
    std::cout << "2 - Suplentes" << std::endl;
    std::cout << "3 - Voltar ao menu principal" << std::endl;

   //ciclo while ate carregar no 3 para sair
    do{
        std::cout << std::endl << "Opcao: ";
        std::cin >> td;   //opção 1, 2 ou 3

        //validar a opção
        //limpamos o buffer de entrada
        if(td<1 || td>3){
            std::cout << "Introduza uma opcao valida! Entre 1 e 3." << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(td<1 || td>3);

    //limpamos o buffer da entrada
    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return td; //retornamos a opção selecionada

}



//introduzir dados futbol
void Gerenciamento::inputDataFut(){
    //variaveis para ler de dados
   size_t size {11};
  // size_t m_scoreTeamB {0};
   //size_t team_size = 0;
    Jogos jogos;
   int tipo = typeData();
       if (tipo ==1 ){
           readDataFut();
       }else if (tipo == 2){
           int tipPl= typePlayer();
           if (tipPl ==1 ){
               //interação com o utilizador
               std::cout << "Entre o nome da equipa A:" << std::endl;
               std::cin >> m_nameTeamA;
               std::cout << " Entre os golos marcados por a equipa A: " << std::endl;
               std::cin >> m_scoreTeamA;
               auto TeamInfoA = jogos.readTeamA(m_nameTeamA, size);
               std::cout << " Entre os golos marcados por a equipa A: " << std::endl;
                //auto supInfoA = jogos.supInfoA(m_supTeamA, m_scoreTeamA);
               std::cout << "Dados introduzidos da Equipa A: " << m_nameTeamA <<" com sussesso" << std::endl;
    //           std::cout << "Entre o nome da equipa B:" << std::endl;
    //           std::cin >> m_nameTeamB;
    //           auto TeamInfoB = jogos.readTeamB(m_nameTeamB, m_scoreTeamB);
    //           std::cout << "Dados introduzidos da Equipa B: " << m_nameTeamB <<" com sussesso" << std::endl;
    //           std::cout << " Entre os golos marcados por a equipa B: " << std::endl;
    //           std::cin >> m_scoreTeamB;
           }else if (tipPl == 2){
               //introduzir suplentes
               suPlayers();
            }
       }

}
// introduzir suplentes
void Gerenciamento::suPlayers(){
    //interação com o utilizador
    Jogos jogos;
    std::cout << "Entre o nome da equipa A:" << std::endl;
    std::cin >> m_nameTeamA;
    auto supInfoA = jogos.readSupA(m_nameTeamA, scoreTeamA);

     //auto supInfoA = jogos.supInfoA(m_supTeamA, m_scoreTeamA);
    std::cout << "Dados dos jogadores suplentes da Equipa A: " << m_nameTeamA <<" introduzidos com sucesso" << std::endl;
//           std::cout << "Entre o nome da equipa B:" << std::endl;
//           std::cin >> m_nameTeamB;
//           auto TeamInfoB = jogos.readTeamB(m_nameTeamB, m_scoreTeamB);
//           std::cout << "Dados introduzidos da Equipa B: " << m_nameTeamB <<" com sucesso" << std::endl;
//           std::cout << " Entre os golos marcados por a equipa B: " << std::endl;
//           std::cin >> m_scoreTeamB;

}
void Gerenciamento::readDataFut(){
         size_t numberPlayer;
         std::string nameTeamA;
         std::string namePlayer;
         std::string posPlayer;
         int scorePlayer = 0;
         int timePlayer = 0;
         std::string nameTeamB;
         int scoreTeamA = 0;
         int scoreTeamB = 0;
         //size_t sizeTeam {11};
         Jogos jogos;
             std::cout << "Ler ficheiro com nome da equipa A:" << std::endl;
             std::cout << "Nome da equipa A: " << std::endl;
             std::cin >> nameTeamA;
             std::ofstream out;
             std::ifstream in;
             std::vector<InfoJogoFutebol> teamInfoA;
             out.open("InfoJogoFutebolA.txt", std::ios_base::app);
             in.open("FutTeamA.txt");
             while(!in.eof()) {
                 int number = -1;
                 std::string name;
                 std::string position;
                 in >> number >> name >> position;
                 out << number << " " << name << " " << position << std::endl;
                 std::cout << number << " " << name << " " << position << std::endl;
                 teamInfoA.emplace_back(numberPlayer, namePlayer,posPlayer,scorePlayer,timePlayer
                                        , nameTeamA, nameTeamB, scoreTeamA, scoreTeamB);
             }
                 std::cout << "Ler ficheiro com nome da equipa B:" << std::endl;
                 std::cout << "Nome da equipa B: " << std::endl;
                 std::cin >> nameTeamB;
                 std::vector<InfoJogoFutebol> teamInfoB;
                 out.open("InfoJogoFutebolB.txt", std::ios_base::app);
                 in.open("FutTeamB.txt");
                 while(!in.eof()) {
                     int number = -1;
                     std::string name;
                     std::string position;

                     in >> number >> name >> position;

                     out << number << " " << name << " " << position << std::endl;
                     std::cout << number << " " << name << " " << position << std::endl;

                     teamInfoA.emplace_back(numberPlayer, namePlayer,posPlayer,scorePlayer,timePlayer
                                            , nameTeamA, nameTeamB, scoreTeamA, scoreTeamB);
                 }

 }





//introduzir dados Basketebol
void Gerenciamento::inputDataBask(){
    std::cout << "Entre dados de basquete \t" << std::endl;
}

void Gerenciamento::showData(){
    //mostra os dados
        std::cout << "imprimejogo futebol"<< std::endl;
        Jogos jogos;
//        auto futebol = InfoJogoFutebol(namePlayer,posPlayer,scorePlayer,timePlayer
//                                       ,nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        //podemos faz<er assim
        InfoJogoFutebol futebol(numberPlayer,namePlayer,posPlayer,scorePlayer,timePlayer
                                ,nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        std::cout << "ImprimeJogo"<< std::endl;
        futebol.imprimeJogo();
        std::cout << "imprimeFut"<< std::endl;
        std::cout << std::endl;
        jogos.imprimeFut();
        std::cout << "Imprime InfoJogo"<< std::endl;
        std::cout << std::endl;
        auto info = InfoJogo(nameTeamA,nameTeamB,scoreTeamA,scoreTeamB);
        info.imprime();
        std::cout << "tamanho do vector" << jogos.m_jogosFutebolA.size();
        std::cout << std::endl;
}

void Gerenciamento::readDataBask(){

}

//std::cout << "Entre o nome dos judagores suplentes da equipa A: " << std::endl;
//std::cout << std::endl;
//for (int i = 1; i < 2; i++){
//    std::cout << "Entre o nome do jogador \t" << i << std::endl;
//    std::cin >> namePlayer;
//    std::cout << "Posição do jogador (guarda-redes, defesa, médio, avançado): \t" << std::endl;;
//    std::cin >> posPlayer;
//    std::cout << i << "Golos marcados por o jogador \t" << namePlayer << std::endl;
//    std::cin >> scorePlayer;
//}
//std::cout << "Introduzir dados da outra equipa " << std::endl;
//std::cout << std::endl;
// introduzir jogadores da equipa B:
//std::cout << "Entre o nome da equipa B:" << std::endl;
//std::cin >> nameTeamB;
//std::cout << "Entre o score da equipa B:" << std::endl;
//std::cin >> scoreTeamB;
//std::cout << "Entre nome dos jogadores titulares da equipa B: \t" << nameTeamB << std::endl;
// introduzir jogadores da equipa B:
//for (int i = 1; i < 4; i++) {
//  std::cout << i << "\n";
//    std::cout << "dados do jogador numero \t" << i << std::endl;
//    std::cout << "Entre o nome do jogador \t" << i << std::endl;
//    std::cin >> namePlayer;
//    std::cout << "Posição do jogador (guarda-redes, defesa, médio, avançado):" << std::endl;;
//    std::cin >> posPlayer;
//    std::cout << i << " \t Golos marcados por o jogador: \t" << namePlayer << std::endl;
//    std::cin >> scorePlayer;
//    std::cout << i << "Em que minuto do jogo o jogador marcou \t" << namePlayer << std::endl;
//    std::cin >> timePlayer;
//    std::cout << std::endl;
//}
//std::cout << "Entre o nome dos jogadores suplentes da equipa B: " << std::endl;
//std::cout << std::endl;
//for (int i = 1; i < 2; i++){
//    std::cout << "Entre o nome do jogador" << i << std::endl;
//    std::cin >> namePlayer;
//    std::cout << "Posição do jogador (guarda-redes, defesa, médio, avançado):" << std::endl;;
//    std::cin >> posPlayer;
//    std::cout << i << "Golos marcados por o jogador" << namePlayer << std::endl;
//    std::cin >> scorePlayer;
//}

