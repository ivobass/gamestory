#include "infojogobasquetebol.h"
#include <iostream>
#include <algorithm>


InfoJogoBasquetebol::InfoJogoBasquetebol(std::string namePlayer, std::string posPlayer, int scorePlayer, int timePlayer,
                                  std::string nameTeamA, std::string nameTeamB, int scoreTeamA, int scoreTeamB)
    : InfoJogo(nameTeamA,nameTeamB,scoreTeamA,scoreTeamB)
{
    m_namePlayer = namePlayer;
    m_posPlayer = posPlayer;
    m_scorePlayer = scorePlayer;
    m_timePlayer = timePlayer;

}


//Getters
std::string InfoJogoBasquetebol::getNamePlayer() const{
    return m_namePlayer;
}

std::string InfoJogoBasquetebol::getPosPlayer() const{
    return m_posPlayer;
}
int InfoJogoBasquetebol::getScorePlayer() const{
    return m_scorePlayer;
}
int InfoJogoBasquetebol::getTimePlayer() const{
    return m_timePlayer;
}

void InfoJogoBasquetebol::imprimeJogo() const{
    std::cout << std::endl;
    std::cout << "Jugador: "<< m_namePlayer << " Posição: " << m_posPlayer << " Pontos Marcados: " << m_scorePlayer
              << " Tempo em que marcou: " << m_timePlayer << std::endl;
}

//Setters
// valida jogadores suplentes


// Imprime resultados jogadores

