TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        Gerenciamento.cpp \
        infojogo.cpp \
        infojogobasquetebol.cpp \
        infojogofutebol.cpp \
        jogos.cpp \
        main.cpp

HEADERS += \
    Gerenciamento.h \
    infojogo.h \
    infojogobasquetebol.h \
    infojogofutebol.h \
    jogos.h
