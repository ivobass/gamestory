#pragma once
#include "infojogofutebol.h"
#include "infojogobasquetebol.h"
#include <string>
#include <vector>
#include <memory>

//class Jogos :  std::vector<InfoJogoFutebol> readTeam(const std::string &nameTeamA, size_t team_size);{
class Jogos  {
public:
Jogos();
 //vector
  std::vector<InfoJogoFutebol> readTeamA(const std::string &nameTeamA, size_t team_size);
  std::vector<InfoJogoFutebol> readSupA(const std::string &supTeamA, size_t sup_size);
  std::vector<InfoJogoFutebol> readTeamB(const std::string &nameTeamA, size_t team_size);
  void adicionarFut(std::unique_ptr<InfoJogoFutebol> valor);
  void adicionarBask(std::unique_ptr<InfoJogoFutebol> valor);
  void ordenaFut();
  void ordenaBask();
  void imprimeFut();
  void imprimeBask();

//  void menu();
//o using std::vector<std::unique_ptr<InfoJogo>>::push_back;

  std::vector<InfoJogoFutebol> teamInfoA;
  std::vector<InfoJogoFutebol> supInfoA;

  std::vector<std::unique_ptr<InfoJogoFutebol>> m_jogosFutebolA;
  std::vector<std::unique_ptr<InfoJogoBasquetebol>> m_jogosBasquetebol;
protected:
//variaveis
  int m_numberPlayer=-1;
  std::string m_namePlayer;
  std::string m_posPlayer;
  int m_scorePlayer;
  int m_timePlayer;
  std::string m_nameTeamA;
  std::string m_nameTeamB;
  int m_scoreTeamA;
  int m_scoreTeamB;
  std::string m_players;
  std::string m_changes;
  std::string nameTeamA;
  std::string nameTeamB;
  int scoreTeamA;
  int scoreTeamB;
};

